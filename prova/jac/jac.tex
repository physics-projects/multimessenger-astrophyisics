%\documentclass[12pt]{report}
\documentclass [11pt,a4paper,twoside, openright]{book}
\usepackage[utf8]{inputenc}
\usepackage{indentfirst}
\usepackage{enumitem}
\usepackage{datetime2}

\usepackage{textcomp}
\usepackage[T1]{fontenc}
\usepackage{multirow}
\usepackage{float}
\usepackage[caption = false]{subfig}
\usepackage{longtable}
\usepackage{listings}
\usepackage{mathtools}
\DeclareMathOperator{\tr}{Tr}
\usepackage{commath}
\usepackage{bbold}
\usepackage{xcolor}
\usepackage{physics}
\usepackage[right=4cm,left=2cm,top=3cm,bottom=3.0cm, marginparwidth=2.7cm, marginparsep=3mm]{geometry}
\usepackage{mdframed}
\usepackage[version=4]{mhchem}

%\usepackage{tikz-cd}
%\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\PassOptionsToPackage{hyphens}{url}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{siunitx}
\sisetup{separate-uncertainty=true}
\DeclareSIUnit\erg{erg}
\DeclareSIUnit\parsec{pc}
\usepackage{cancel}
\usepackage{mathrsfs}
\usepackage{tensor}
\usepackage{marginnote}
\renewcommand*{\marginnotevadjust}{-0.3cm}
\renewcommand*{\marginfont}{\scriptsize}
% \usepackage{fancybox}

\usepackage{footnotebackref}

\usepackage[sc]{mathpazo}
\linespread{1.05}         % Palladio needs more leading (space between lines)
\usepackage[T1]{fontenc}

\newcommand{\diag}[1]{\text{diag}\qty(#1)}
\newcommand{\const}{\text{const}}
\newcommand{\sign}{\text{sign}}
\renewcommand{\H}{\mathcal{H}}
\renewcommand{\dim}{\text{dim}}
\newcommand{\supp}[1]{\text{supp} \qty(#1)}

\usepackage{nicefrac}
\usepackage{ifthen}
\let\oldfrac\frac
\renewcommand{\frac}[3][d]{\ifthenelse{\equal{#1}{d}}{\oldfrac{#2}{#3}}{\nicefrac{#2}{#3}}}

\renewcommand{\var}[1]{\text{var} \qty(#1)}
\newcommand{\defeq}{\ensuremath{\stackrel{\text{def}}{=}}}

\newcommand\mybox[1]{%
  \fbox{\begin{minipage}{0.9\textwidth}#1\end{minipage}}}

%Spiegazioni/verifiche
\newenvironment{greenbox}{\begin{mdframed}[hidealllines=true,backgroundcolor=green!20,innerleftmargin=3pt,innerrightmargin=3pt]}{\end{mdframed}}

%Approfondimenti
\newenvironment{bluebox}{\begin{mdframed}[hidealllines=true,backgroundcolor=blue!10,innerleftmargin=3pt,innerrightmargin=3pt]}{\end{mdframed}}

\newtheorem{claim}{Claim}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{proposition}{Proposition}[section]

%\usepackage{circledsteps}

\newcommand{\hlc}[2]{%
  \colorbox{#1!50}{$\displaystyle#2$}}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
  backgroundcolor=\color{backcolour},
  commentstyle=\color{codegreen},
  keywordstyle=\color{magenta},
  numberstyle=\tiny\color{codegray},
  stringstyle=\color{codepurple},
  basicstyle=\ttfamily\footnotesize,
  breakatwhitespace=false,
  breaklines=true,
  captionpos=b,
  keepspaces=true,
  numbers=left,
  numbersep=5pt,
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  tabsize=2
}

\lstset{style=mystyle}
\usepackage{svg}

\usepackage{pgf,tikz}
\usetikzlibrary{arrows}
% \pagestyle{empty}
\definecolor{uququq}{rgb}{0.25,0.25,0.25}

\usepackage[activate={true,nocompatibility},final,tracking=true,kerning=true,factor=1100,stretch=10,shrink=10]{microtype}

\newcommand{\boxalign}[2][0.986\textwidth]{
  \par\noindent\tikzstyle{mybox} = [draw=black,inner sep=6pt]
  \begin{center}\begin{tikzpicture}
   \node [mybox] (box){%
    \begin{minipage}{#1}{\vspace{-5mm}#2}\end{minipage}
   };
\end{tikzpicture}\end{center}}

\author{Jacopo Tissino, Giorgio Mentasti}

\allowdisplaybreaks
%------------------------------------------------------------------------------------------------

\begin{document}
\marginpar{Friday\\ 2020-3-13, \\ compiled \\ \today}

Today we introduce the concept of \emph{invariant mass}: we consider a system of \(N\) particles, each with energy \(E_{k}\) and momentum \(\vec{p}_{k}\). 
Their four-momenta are \(p_{k} = \qty(E_{k} / c, \vec{p}_{k})\). 

The total 4-momentum is 
%
\begin{align}
p_{\text{tot}} = \sum_{k=1}^{N} p_{k}
\,.
\end{align}

The scalar product between two of these is defined as 
%
\begin{align}
p_1 \cdot p_{2} = \frac{E_1 E_2 }{c^2} - \vec{p}_{1} \cdot \vec{p}_{2}
\,.
\end{align}

We define the quantity 
%
\begin{align}
\sqrt{s} = \sqrt{p^2 _{\text{tot}}}
= \sqrt{\big(\sum _{k} E_{k}\big)^2 - \big|\sum _{k} \vec{p}_{k}\big| c^2}
\,.
\end{align}

The variable \(s\) is called the center-of-mass energy, or invariant mass. 

Let us consider a fixed target experiment: a particle 1 incident upon a screen, call it 2. 
Let us consider the frame in which the screen is at rest. 

The momenta are \(p_1 = \qty(E_1 / c,  \vec{p}_{1})\) and \(p_2 = \qty(m_2 c, \vec{0})\). 
The total momentum is then 
%
\begin{align}
p _{\text{tot}} = \qty(\frac{E_1}{c} + m_2 c , \vec{p}_{1})
\,,
\end{align}
%
so 
%
\begin{align}
\sqrt{s} = \qty(\qty(E_1 + m_2 c^2)^2 - p_1^2)
\,,
\end{align}
%
so 
%
\begin{align}
\sqrt{s} = \sqrt{m_1^2c^{4} + m_2^2c^{4} + 2 E_1 m_2 c^2}
\,.
\end{align}

If, instead, both particles are moving then  
%
\begin{align}
\sqrt{s} = \sqrt{m_1^2c^{4} + m_2^2c^{4} + 2 \qty(E_1 E_2 - \abs{p_1 } \abs{p_2 } c^2 \cos \theta )}
\,,
\end{align}
%
where \(\theta \) is the angle between the particles' trajectories. For a head-on collision 
%
\begin{align}
\sqrt{s} = 
\sqrt{m_1^2c^{4} + m_2^2c^{4} + 2 \qty(E_1 E_2 - \abs{p_1 } \abs{p_2 } c^2)}
\,.
\end{align}

The flux of cosmic particles decreases approximately as a powerlaw with the energy \cite[eq. 30.2]{patrignaniCosmicRays}: 
%
\begin{align}
\text{flux} \approx \num{1.8e4} \qty(\frac{E}{\SI{1}{GeV}})^{-\alpha }  \frac{\text{nucleons}}{\SI{}{m^2 sr s GeV}}
\,,
\end{align}
%
with \(\alpha \approx \num{2.7}\) generally, however there are high energy regions where this slope changes slightly.

We can detect cosmic rays at energies higher than the maximum ones of the LHC. 
The LHC COM energy is reported to be around \SI{e17}{GeV}: this seems strange!
It is actually a consequence of the difference between fixed-target experiments (which describes the interaction between a cosmic ray and a nucleus on the Earth well) and experiments such as the LHC, in which two beams collide head on.

\paragraph{Exercise} at the LHC the particles can be accelerated to energies of around
%
%\begin{align}
%E \approx 7 \text{TeV}
%\end{align}
%
so we want to compute the COM energy \(\sqrt{s}\) for two protons colliding head-on, and we wish to compute the energy a cosmic ray would need to have in order to have \(\sqrt{s}\) equal to that of the LHC. 

For the LHC two protons with \(E \approx \SI{7}{TeV}\) collide, so the total COM energy is \(\sqrt{s} \approx \SI{14}{TeV}\). 

Now, for the fixed target collision: the formula will be, taking proton \(1 \) to be the cosmic ray and proton \(2\) to be the stationary proton, which has \(\vec{p}_{2} = 0\) and \(\vec{E}_{2}  = m_p\) (setting \(c=1\)):  
%
\begin{align}
\sqrt{s} &= \sqrt{\qty(E_1 + E_2 )^2 + \abs{\vec{p}_1 + \vec{p}_2}^2 }  \\
&= \sqrt{E_1^2 + 2 E_1 m_p + m_p^2 + m_p^2 - E_1^2}  \\
&= \sqrt{2 m_p^2 + 2 E_1 m_p} 
\,,
\end{align}
%
so by setting \(\sqrt{s} = \SI{14}{TeV}\) we find: 
%
\begin{align}
s &=  \qty(\SI{14}{TeV})^2= 2 \qty(E_1 m_p + m_p^2)  \\
E_1 &= \frac{s - 2 m_p^2}{2 m_p} \approx \SI{e8}{GeV}
\,.
\end{align}

\subsection{Interactions with matter}

We wish to compute 
\begin{enumerate}
  \item the probability that a particle survives an interaction with matter;
  \item and its \emph{mean free path}: the average distance a particle travels before interacting. 
\end{enumerate}

These are both related to the \emph{cross section}: it quantifies the probability of a certain interaction to occur between two particles --- in which case we refer to a \emph{partial} cross section --- or of any interaction happening between the two particles, in which case we talk about a total cross section. 

For clarity, let us focus on a specific example: a beam colliding on a target. 
We suppose that the beam is much broader than the target (so we can ignore border effects), and that the particles are uniformly distributed both in space and time. 

Then, we define the incident flux \(F\) by:
%
\begin{align}
F = \frac{\text{\# particles}}{ \dd{t} \dd{A}}
\,,
\end{align}
%
and is measured in \SI{}{s^{-1} m^{-2}}.
We want to consider the particles which are scattered in the solid angle \(\dd{\Omega }\). 
Scattering is intrinsically stochastic, so we will be looking at averages: specifically, we are interested in the average number of particles scattered per unit solid angle per unit time: 
%
\begin{align}
\expval{\frac{ \dd{N}_{s}}{ \dd{\Omega }}}
\,;
\end{align}
%
for simplicity hereafter we will omit the signs of average, but they will always be implied. This quantity is measured in \SI{}{sr^{-1} s^{-1}}.

\todo[inline]{There was no unit time mentioned by the professor, but we need it for the dimensions to work.}

The differential cross section \(\dv*{\sigma }{\Omega }\), which depends on the energy of the incoming particle and on the direction of scattering, is then defined as: 
%
\begin{align}
\dv{\sigma }{\Omega } (E, \Omega ) = \frac{1}{F} \dv{N_{s}}{\Omega }
\,.
\end{align}


The total cross section is defined as 
%
\begin{align}
\sigma (E) = \int_{S^{2}} \dv{\sigma }{\Omega } \dd{\Omega }
\,.
\end{align}

This is dimensionally an area: it quantifies the ``size of the target'' which must be hit in order to have an interaction.

Typically we deal with slabs of material with many scattering centers. 
So, we can define the number of scattering centers per unit beam area (dimensionally, an inverse area): 
%
\begin{align}
n \delta x 
\,,
\end{align}
%
where \(n\) is the number density of the beam, measured in \SI{}{m^{-3}}, and \(\delta x \) is the thickness of the slab along the beam direction.

The number of particles per unit time which can interact is given by \(F A\), where \(F\) is the flux while \(A\) is the cross-sectional area of the beam. 
The dimensions of \(FA\) are those of an inverse time.

Then, the average number of particles scattered per unit time and solid angle is given by 
%
\begin{align}
\dv{N_s}{\Omega } (\Omega ) =  FA N \delta x \dv{\sigma }{\Omega }
\,,
\end{align}
%
while the total number of particles scattered per unit time is 
%
\begin{align}
N_{s} = FA N \delta x \sigma 
\,.
\end{align}

The probability of a particle in the beam to interact while going through a layer of thickness \(\delta x\) will then be given by 
%
\begin{align} \label{eq:probability-of-interaction}
\mathbb{P} = 
\frac{\text{particles which do scatter per unit time}}{\text{available particles to scatter per unit time}}
= \frac{N_s}{FA} = N \sigma  \delta x 
\,.
\end{align}

We are usually interested in characterizing the probability 
of interaction in a thick layer.
The probability of interacting in a large thickness \(x\) is given by 
%
\begin{align}
\mathbb{P} _{\text{interaction}} = 1 - \mathbb{P}_{\text{survival}}
\,,
\end{align}
%
where \(\mathbb{P}_{\text{survival}}\) quantifies the probability that the particle will not interact, i.e. ``survive''. 

The probability of interaction in the region \(x\) to \(x + \dd{x}\): this will be a multiple of \(\dd{x}\), which we call \(\mathbb{P} =  w \dd{x}\).

Since the events are independent,
the probability of surviving up to a depth \(x + \dd{x}\) will then be given by 
%
\begin{align}
\mathbb{P} (x + \dd{x}) &= 
\mathbb{P} (x) (1 - w \dd{x})  \\
&= \mathbb{P} (x) + \dv{\mathbb{P} }{x} \dd{x}
\,,
\end{align}
%
which means 
%
\begin{align}
\dd{\mathbb{P}} = - w \dd{x}
\,,
\end{align}
%
therefore, if the probability is normalized so that \(\mathbb{P}(0) =1\), we have 
%
\begin{align}
\mathbb{P}(x) =  e^{- \int w \dd{x}}
\,.
\end{align}

Then, the probability of interaction will be 
%
\begin{align}
\mathbb{P} _{\text{int}} = 1 - e^{- \int w \dd{x}}
\,.
\end{align}

The mean free path is calculated as 
%
\begin{align}
\lambda  = \expval{x} = \frac{\int x \mathbb{P} (x) \dd{x}}{\int \mathbb{P} (x) \dd{x}}
\,,
\end{align}
%
and by expanding the exponential we can see that \(w = 1 / \lambda \). 

Since we also have the relation \(\dd{\mathbb{P} } = - w \dd{x} = -N \sigma \dd{x}\) (minus sign since we are considering survival, not scattering), we can identify 
%
\begin{align}
w = \frac{1}{\lambda } = N \sigma 
\,,
\end{align}
%
therefore 
%
\begin{align}
\lambda = \frac{1}{N \sigma }
\,.
\end{align}

The number density of scattering centers can be derived as 
%
\begin{align}
N = N_A \frac{\rho }{\omega_{A}}
\,,
\end{align}
%
where \(N_A \approx \SI{6.022e23}{mol^{-1}}\) is Avogadro's number, while \(\omega_{A}\) is the mean molecular weight.

If a beam of particles crosses matter, its intensity will be reduced. If \(N\) is the number of particles in the beam, it will decrease in a length \(\dd{x}\) by 
%
\begin{align}
\frac{\dd{N}}{N} = \rho \frac{N_A}{\omega_{A}} \sigma \dd{x}  
\,.
\end{align}

Often we define a different quantity, similar to the mean free path: 
%
\begin{align}
\lambda' =\rho \lambda = \frac{\rho \omega_{A} }{\sigma \rho  N_A}= \frac{\omega_{A} }{\sigma  N_A}
\,,
\end{align}
%
which is measured in \SI{}{kg /m^2}. This is useful since often we need to deal with mediums of variable density, so it is easier if we do not need to account for it.

\section{Interactions of astroparticles}

\todo[inline]{change section name?}

We must be able to describe the whole lifetime of the astroparticle: the processes affecting it in the astrophysical environment, close to the source 1; the processes happening in the atmosphere 2. 

In the Moodle there is a figure for the opacity of the atmosphere for different photon energies. 

Also, we must describe the processes happening when we are observing the particles. This is described by the physics of particle detectors 3.

We start with 3.

\subsection{Astroparticles and detectors}

We must consider nuclei (cosmic rays), photons, neutrinos and gravitational waves. 
We will neglect gravitational waves. 

For nuclei, we need to account for the strong, weak and electromagnetic interactions; the strong interaction is the dominant process.  

For photons, we just need to account for the electromagnetic interaction. 

For neutrinos, we just need to account for the weak interaction.

For a proton-proton interaction, the strong-interaction cross section is typically \(\sigma_{pp} \approx \SI{45}{mb}\), where \(\SI{}{b} = \SI{e-28}{m^2}\) is a barn, the approximate area of a uranium nucleus.

We can read a research article, in which they measure the proton-air cross section \cite[]{pierreaugercollaborationMeasurementProtonairCrosssection2012}.
In this work, figure 2 shows the cross section of proton-air interaction. It is on the order of the hundreds of millibarn, increasing with energy.

We can calculate the quantity \(\lambda'\) we defined before: 
%
\begin{align}
\lambda' = \frac{\omega_{A}}{ N_A \sigma } \approx \SI{93}{g / cm^2}
\,.
\end{align}

This is much lower than \(\rho _{\text{atm}} / L _{\text{atm}}\): therefore a cosmic ray will typically not survive to the ground. 

Let us present a simple model to derive the proton-proton cross section if we have the proton-air one. 
We approximate the proton-air cross section to be constant in energy. 
We can say that, to first approximation, \(\sigma = \pi R^2\) with \(R = R_T + R_p\).
The radius of an atomic nucleus can be approximated by 
%
\begin{align}
r_T^{(A)} = r_0 A^{1/3} 
\,,
\end{align}
%
where \(r_0 \approx \SI{1.2}{fm}\). 
Using this, we can get an estimate of the proton-proton cross section, to be compared with collider experiments. 

Moving on to 2: the typical interaction length for pair production is given by 
%
\begin{align}
\lambda_{\gamma  } = \frac{9}{7} x_0 
\,,
\end{align}
%
since electrons interact in matter by giving off brehmssrahlung radiation, a process which is related to pair production. 
The quantity \(x_0 \approx \SI{36}{g/ cm^2}\): the radiation length for electrons in matter. So, \(\lambda_{\gamma } \approx \SI{47}{g/cm^2 }\) for \(E > \SI{10}{GeV}\).

In order to get information about these quantities, we can use data from the Particle Data Group, at \url{http://pdg.lbl.gov}.
\end{document}