import numpy as np
import subprocess
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
from matplotlib import rcParams
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text',usetex=True)
rcParams.update({'figure.autolayout': True})

def PowerTen(num,pos=None):
	if (num==0.):
		return "$0$"
	decimal_digits=2
	exponent=int(np.floor(np.log10(abs(num))))
	coeff=round(num/float(10**exponent),decimal_digits)
	if (exponent==0.):
		return "${:.1f}$".format(coeff)
	if (coeff==1.):
		return r"$10^{{{:d}}}$".format(exponent)
	else:
		return r"${0:.2f}\times10^{{{1:d}}}$".format(coeff,exponent)

def box_func (x,func,min_func,max_func):
	box=np.zeros(len(x),float)
	for j in range (0,len(box)):
		if(x[j]<min_func):
			box[j]=0.
		elif(x[j]>max_func):
			box[j]=0.
		else:
			box[j]=func
	return box

#---------------------------------------------------------------------------------------------
#The plots

#DN/DE energy spectrum
fig=plt.figure(1,figsize=(15,6))
uno=plt.subplot(121)
E_gamma,E_pi=np.linspace(1e-5,100,5000000),np.array([0.2,1,5,10,50])
vertical=np.linspace(0.,1e2,1e2)
m_pi=135e-3																#Gev/c^2
dN_dE=lambda E_pi: 1./((E_pi**2.-m_pi**2.)**(1./2.)) 
E_gamma_max=lambda E_pi: E_pi/2.*(1.+((E_pi**2.-m_pi**2.)**(1./2.))/(E_pi))
E_gamma_min=lambda E_pi: E_pi/2.*(1.-((E_pi**2.-m_pi**2.)**(1./2.))/(E_pi))
E_gamma_mid=67.5*0.001
markers=['-', '--', '-.', ':','-']
plt.plot(vertical*0+E_gamma_mid,vertical,"r-", \
label="$E_{\\gamma}^{\mathrm{mid}}=67.5\:\mathrm{MeV}$")
step_functions=np.zeros([len(E_pi),len(E_gamma)],float)

for i in range(0,len(E_pi)):
	print("Spectrum=",dN_dE(E_pi[i])," E_pi=", E_pi[i]," min=",\
	E_gamma_min(E_pi[i])," max=",E_gamma_max(E_pi[i]))
	step=box_func(E_gamma,dN_dE(E_pi[i]),E_gamma_min(E_pi[i]),E_gamma_max(E_pi[i]))
	step_functions[i]=step
	plt.step(E_gamma, step, color="k", linestyle=markers[i], \
	label="$E_{{\\pi}}={}\:\mathrm{{GeV}}$".format(E_pi[i]))
	
plt.xlabel('$E_{\\gamma}\:[\mathrm{GeV}]$',fontsize=20)
plt.ylabel('$\\frac{\mathrm{d}N_{\\gamma}}{\mathrm{d}E_{\\gamma}}\:[\mathrm{GeV}^{-1}\:\mathrm{cm}^{-2}\:\mathrm{s}^{-1}]$'\
,fontsize=20)
plt.yscale("log")
plt.xscale("log")
plt.axis([1e-5,1e2,1e-2,1e2])
plt.legend(loc="best",frameon=False,prop={'size': 17})
plt.tight_layout()
plt.tick_params(labelsize=20)

due=plt.subplot(122)
for i in range(0,len(E_pi)):
	plt.plot(E_gamma,step_functions[i]*E_gamma**2,color="k",linestyle=markers[i],\
	label="$E_{{\\pi}}={}\:\mathrm{{GeV}}$".format(E_pi[i]))

plt.xlabel('$E_{\\gamma}\:[\mathrm{GeV}]$',fontsize=20)
plt.ylabel('$E_{\\gamma}^{2}\:\\frac{\mathrm{d}N_{\\gamma}}{\mathrm{d}E_{\\gamma}}\:[\mathrm{GeV}\:\mathrm{cm}^{-2}\:\mathrm{s}^{-1}]$'\
,fontsize=20)
plt.legend(loc="best",frameon=False,prop={'size': 17})
plt.yscale("log")
plt.xscale("log")
plt.tight_layout()
plt.tick_params(labelsize=20)
plt.savefig("SED.pdf",format="pdf")		
subprocess.Popen(["evince","SED.pdf"])
