import matplotlib.pyplot as plt
import subprocess
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text',usetex=True)
rcParams.update({'figure.autolayout': True})

def PowerTen(num,pos=None):
	if (num==0.):
		return "$0$"
	decimal_digits=2
	exponent=int(np.floor(np.log10(abs(num))))
	coeff=round(num/float(10**exponent),decimal_digits)
	if (exponent==0.):
		return "${:.1f}$".format(coeff)
	if (coeff==1.):
		return r"$10^{{{:d}}}$".format(exponent)
	else:
		return r"${0:.2f}\times10^{{{1:d}}}$".format(coeff,exponent)
		
def relax(alpha,tol):	
	x=0.1
	x_guess=1.
	iter_number=0
	while(abs(x-x_guess)>tol):
		x_guess=x
		x=(1+alpha*(np.e)**(-x)-alpha)/(1+2*alpha*(np.e)**(-x))		
		#if(iter_number==0):
			#print("\n--------------------------------------------Relaxation method---------------------------------------")	
		#print("Iteration number", iter_number,"result is:",x)
		iter_number+=1
	return(x,iter_number)
		
#---------------------------------------------------------------------------------------------
#The plots 

#Probability for a pion to interact
plt.figure(1,figsize=plt.figaspect(0.75))
lambda_I=100													#g cm^{-2}
prob=lambda x: x*(np.e)**(-x/lambda_I)/(lambda_I)**2   
x,y=np.linspace(0,1000,10000),np.linspace(0,0.45,100)
max_x,prev_prob=0.,0.
for i in range(0,len(x)):	
	if (prob(x[i])>prev_prob):
		max_x=x[i]
		prev_prob=prob(x[i])

plt.plot(x/lambda_I,prob(x)*lambda_I,'k-',label="$\mathrm{d}P_I^\pi/\mathrm{d}x$")
plt.plot(y*0+max_x/lambda_I,y,"k--",label="$x_{\mathrm{max}}$" )
plt.text(5,0.2, \
"$x_\mathrm{{max}}=${}$\:\mathrm{{g}}\:\mathrm{{cm}}^{{-2}}$".format(PowerTen(max_x)),fontsize=25)
plt.xlabel('$x/\lambda_I$',fontsize=25)
plt.ylabel('$\mathrm{d}P_I^\pi(x)/\mathrm{d}x$',fontsize=25)
plt.legend(loc="best",frameon=False,prop={'size': 23})
plt.tight_layout()
plt.tick_params(labelsize=25)
plt.savefig("pion_probability.pdf",format="pdf")		
#subprocess.Popen(["evince","pion_probability.pdf"])

#Probability for a pion to decay 
plt.figure(2,figsize=plt.figaspect(0.75))
plt.axis([0.,7.,-0.05,1.1])
alphas=np.array([0,1,1000])
prob_decay=lambda x,alpha: (alpha/(alpha+1))/lambda_I*(np.e)**(-x/lambda_I)
plt.plot(x/lambda_I,prob_decay(x,alphas[0])*lambda_I,'k-',label="$\\alpha={}$".format(alphas[0]))
plt.plot(x/lambda_I,prob_decay(x,alphas[1])*lambda_I,'k--',label="$\\alpha={}$".format(alphas[1]))
plt.plot(x/lambda_I,prob_decay(x,alphas[2])*lambda_I,'k-.',label="$\\alpha={}$".format(alphas[2]))
plt.xlabel('$x/\lambda_I$',fontsize=25)
plt.ylabel('$\mathrm{d}P_d^\pi(x)/\mathrm{d}x$',fontsize=25)
plt.legend(loc="best",frameon=False,prop={'size': 23})
plt.tight_layout()
plt.tick_params(labelsize=25)
plt.savefig("pion_probability_decay.pdf",format="pdf")	
#subprocess.Popen(["evince","pion_probability_decay.pdf"])		

#Probability for a pion to decay or interact
plt.figure(3,figsize=plt.figaspect(0.75))
plt.axis([0.,7.,0,1.1])
prob_tot=lambda x,alpha: (1/(alpha+1)*x/lambda_I+alpha/(alpha+1))/lambda_I*(np.e)**(-x/lambda_I)
prob_tot2=lambda x,alpha: (1/(alpha+1)*x/lambda_I+alpha/(alpha+1))/lambda_I*(np.e)**(-x/lambda_I)+\
(x/(lambda_I)**2)*(alpha/(alpha+1))*(np.e)**(-2*x/lambda_I)
plt.plot(x/lambda_I,prob_tot(x,alphas[0])*lambda_I,'k-',label="$\\alpha={}$".format(alphas[0]))
plt.plot(x/lambda_I,prob_tot(x,alphas[1])*lambda_I,'k--',label="$\\alpha={}$".format(alphas[1]))
plt.plot(x/lambda_I,prob_tot(x,alphas[2])*lambda_I,'k-.',label="$\\alpha={}$".format(alphas[2]))
#corrections plot
plt.plot(x/lambda_I,prob_tot2(x,alphas[0])*lambda_I,'r-',\
label="$\\alpha_{{\mathrm{{corr}}}}={}$".format(alphas[0]))
plt.plot(x/lambda_I,prob_tot2(x,alphas[1])*lambda_I,'r--',\
label="$\\alpha_{{\mathrm{{corr}}}}={}$".format(alphas[1]))
plt.plot(x/lambda_I,prob_tot2(x,alphas[2])*lambda_I,'r-.',\
label="$\\alpha_{{\mathrm{{corr}}}}={}$".format(alphas[2]))
plt.xlabel('$x/\lambda_I$',fontsize=25)
plt.ylabel('$\mathrm{d}P^\pi(x)/\mathrm{d}x$',fontsize=25)
plt.legend(loc="best",frameon=False,prop={'size': 23})
plt.tight_layout()
plt.tick_params(labelsize=25)
plt.savefig("pion_probability_total.pdf",format="pdf")		
#subprocess.Popen(["evince","pion_probability_total.pdf"])

#x at which 1/e survived
plt.figure(4,figsize=plt.figaspect(0.75))
plt.axis([0.,4.,-80,40])
function=lambda x, alpha: 1/(np.e)-((np.e)**(-x/lambda_I)*(1+alpha+(x/lambda_I)))/(1+alpha)
zero_axis=x*0+0
plt.plot(x/lambda_I,zero_axis,"b-")
plt.plot(x/lambda_I,function(x,alphas[0])*lambda_I,'k-',label="$\\alpha={}$".format(alphas[0]))
plt.plot(x/lambda_I,function(x,alphas[1])*lambda_I,'k--',label="$\\alpha={}$".format(alphas[1]))
plt.plot(x/lambda_I,function(x,alphas[2])*lambda_I,'k-.',label="$\\alpha={}$".format(alphas[2]))
abscissa_survived=np.zeros(len(alphas),float)
i=0
while (i<=2):
	for j in range(0,len(x)):
		#print(alphas[i],function2(x[j],alphas[i]))
		if(abs(function(x[j],alphas[i]))<1e-3):
			abscissa_survived[i]=x[j]
			print("Non corrected values:",alphas[i],abscissa_survived[i]/lambda_I)
			plt.plot(abscissa_survived[i]/lambda_I,0.,"*", color="b", \
			markeredgecolor="b", markersize=12)
			i+=1
			break

plt.xlabel('$x/\lambda_I$',fontsize=25)
plt.ylabel('$(1-1/e)-P^\pi(x)$',fontsize=25)
plt.legend(loc="best",frameon=False,prop={'size': 23})
plt.tight_layout()
plt.tick_params(labelsize=25)
plt.savefig("pion_survived_fraction.pdf",format="pdf")		
#subprocess.Popen(["evince","pion_survived_fraction.pdf"])

#x at which 1/e survived CORRECTED
plt.figure(5,figsize=plt.figaspect(0.75))
plt.axis([0.,4.,-80,40])
function2=lambda x, alpha: 1/(np.e)-((np.e)**(-x/lambda_I)*(1+alpha+(x/lambda_I)))/(1+alpha)+\
(alpha/(alpha+1))*(1/4)*(-(2*x/lambda_I)*(np.e)**(-2*x/lambda_I)-(np.e)**(-2*x/lambda_I)+1)
zero_axis=x*0+0
plt.plot(x/lambda_I,zero_axis,"r-")
plt.plot(x/lambda_I,function2(x,alphas[0])*lambda_I,'k-',\
label="$\\alpha_{{\mathrm{{corr}}}}={}$".format(alphas[0]))
plt.plot(x/lambda_I,function2(x,alphas[1])*lambda_I,'k--',\
label="$\\alpha_{{\mathrm{{corr}}}}={}$".format(alphas[1]))
plt.plot(x/lambda_I,function2(x,alphas[2])*lambda_I,'k-.',\
label="$\\alpha_{{\mathrm{{corr}}}}={}$".format(alphas[2]))
abscissa_survived_corr=np.zeros(len(alphas),float)
i=0
while (i<=2):
	for j in range(0,len(x)):
		if(abs(function2(x[j],alphas[i]))<1e-3):
			abscissa_survived_corr[i]=x[j]
			print("Corrected values:",alphas[i],abscissa_survived_corr[i]/lambda_I)
			plt.plot(abscissa_survived_corr[i]/lambda_I,0.,"*", color="r", \
			markeredgecolor="r", markersize=12)
			i+=1
			break

plt.xlabel('$x/\lambda_I$',fontsize=25)
plt.ylabel('$(1-1/e)-P^\pi(x)$',fontsize=25)
plt.legend(loc="best",frameon=False,prop={'size': 23})
plt.tight_layout()
plt.tick_params(labelsize=25)
plt.savefig("pion_survived_fraction_CORR.pdf",format="pdf")		
#subprocess.Popen(["evince","pion_survived_fraction_CORR.pdf"])

#Probability for a pion to interact CORRECTED
plt.figure(7,figsize=plt.figaspect(0.75))
lambda_I=100													#g cm^{-2}
prob_intCORR=lambda x, alpha: x*(np.e)**(-x/lambda_I)/(lambda_I)**2*(1/(alpha+1))
plt.plot(x/lambda_I,prob_intCORR(x,alphas[0])*lambda_I,'r-',\
label="$\\alpha={}$".format(alphas[0]))
plt.plot(x/lambda_I,prob_intCORR(x,alphas[1])*lambda_I,'r--',\
label="$\\alpha={}$".format(alphas[1]))
plt.plot(x/lambda_I,prob_intCORR(x,alphas[2])*lambda_I,'r-.',\
label="$\\alpha={}$".format(alphas[2]))
plt.plot(x/lambda_I,prob_intCORR(x,10)*lambda_I,'r-.',\
label="$\\alpha={}$".format(10))
plt.plot(x/lambda_I,prob(x)*lambda_I,'k-',\
label="$\mathrm{d}P_I^\pi/\mathrm{d}x$")
plt.plot(y*0+max_x/lambda_I,y,"k--",label="$x_{\mathrm{max}}=100\:\mathrm{g\: cm}^{-2}$" )
#plt.text(5,0.2, \
#"$x_\mathrm{{max}}=${}$\:\mathrm{{g}}\:\mathrm{{cm}}^{{-2}}$".format(PowerTen(max_x)),fontsize=25)
plt.xlabel('$x/\lambda_I$',fontsize=25)
plt.ylabel('$\mathrm{d}P_I^\pi(x)/\mathrm{d}x$',fontsize=25)
plt.legend(loc="best",frameon=False,prop={'size': 23})
plt.tight_layout()
plt.tick_params(labelsize=25)
plt.savefig("pion_probability_interactionCORR.pdf",format="pdf")		
#subprocess.Popen(["evince","pion_probability_interactionCORR.pdf"])

#Relaxation method for maximum atompsheric depht-----------------------------------------------
max_atmospheric=np.zeros(len(alphas),float)
tolerance=1e-6
for i in range(0,len(alphas)):
	(result,iterations)=relax(alphas[i],tolerance)
	max_atmospheric[i]=result
	print("The xmax from the relaxation method after",iterations, \
	"iterations is:",result)



#----------------------------------------------------------------------------------------------

#Greisen profile
plt.figure(6,figsize=plt.figaspect(0.75))
plt.axis([0.,35.,0,10])
x_0=37.1*np.log(2)
greisen=lambda x,E,s: (0.31/(np.sqrt(np.log(E))))*(np.e)**((1.-3./2.*np.log(s))*x)
s_func=lambda x,E: 3*x/(x+2*(np.log(E)))	
s_fixed=np.array([0.5,1,1.3])
E_fixed_log=np.array([6,10,14,18,22,24])
E_fixed, E_vector=np.array([0.035*10**(12),2.*10**(12),100*10**(12), \
5.7*10**(15),320*10**(15),2300*10**(15)]), np.linspace(0.02*10**(12),3500*10**(15),1000)
E_0=85*10**(6)
x_axis=np.linspace(0.1,35,1000)
for i in range(0,len(E_fixed)):
	mass_y=max(np.log10(greisen(x_axis,E_fixed[i]/E_0,s_func(x_axis,E_fixed[i]/E_0))))
	mass_x,prev_greisen=0.,0.
	for j in range(0,len(x_axis)):	
		if (np.log10(greisen(x_axis[j],E_fixed[i]/E_0,s_func(x_axis[j],\
		E_fixed[i]/E_0)))>prev_greisen):
			mass_x=x_axis[j]
			prev_greisen=np.log10(greisen(x_axis[j],E_fixed[i]/E_0,\
			s_func(x_axis[j],E_fixed[i]/E_0)))
	plt.plot(mass_x,mass_y,"*",color="#17a1bd",markeredgecolor="#17a1bd", markersize=12)
	plt.plot(x_axis,np.log10(greisen(x_axis,E_fixed[i]/E_0,s_func(x_axis,E_fixed[i]/E_0))),'r-',\
	label="$k={}$".format(E_fixed_log[i]))
for i in range(0,len(s_fixed)):
	if(i==1):
		plt.plot(x_axis,np.log10(greisen(x_axis,E_vector/E_0,s_fixed[i])),'-',\
		color="#17a1bd",linewidth=3)	
	plt.plot(x_axis,np.log10(greisen(x_axis,E_vector/E_0,s_fixed[i])),'-',color="#17a1bd")
plt.text(17,0.5,"${}$".format(E_fixed_log[0]), fontsize=20,color="r")
plt.text(31,0.75,"${}$".format(E_fixed_log[1]),fontsize=20,color="r")
plt.text(32,3.5,"${}$".format(E_fixed_log[2]),fontsize=20,color="r")
plt.text(32,6.,"${}$".format(E_fixed_log[3]),fontsize=20,color="r")
plt.text(32,8.25,"${}$".format(E_fixed_log[4]),fontsize=20,color="r")
plt.text(32,9.15,"${}$".format(E_fixed_log[5]),fontsize=20,color="r")
plt.text(6,9.2,"$\mathrm{s}=0.5$",fontsize=20,color="#17a1bd")
plt.text(15,7,"$\mathrm{s}=1$",fontsize=20,color="#17a1bd")
plt.text(26,7,"$\mathrm{s}=1.3$",fontsize=20,color="#17a1bd")
plt.ylabel('$\log_{10}N_e^\gamma$',fontsize=25)
plt.xlabel('$t=x/\mathrm{x}_0$',fontsize=25)
#plt.legend(loc="best",frameon=False,prop={'size': 23})
plt.tight_layout()
plt.tick_params(labelsize=25)
plt.savefig("greisen_profile.pdf",format="pdf")		
#subprocess.Popen(["evince","greisen_profile.pdf"])
