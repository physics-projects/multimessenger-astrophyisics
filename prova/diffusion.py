import matplotlib.pyplot as plt
import subprocess
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text',usetex=True)
rcParams.update({'figure.autolayout': True})

def PowerTen(num,pos=None):
	if (num==0.):
		return "$0$"
	decimal_digits=2
	exponent=int(np.floor(np.log10(abs(num))))
	coeff=round(num/float(10**exponent),decimal_digits)
	if (exponent==0.):
		return "${:.1f}$".format(coeff)
	if (coeff==1.):
		return r"$10^{{{:d}}}$".format(exponent)
	else:
		return r"${0:.2f}\times10^{{{1:d}}}$".format(coeff,exponent)

#---------------------------------------------------------------------------------------------
#The plots

#ESCAPE-TIME: abundances light-medium
plt.figure(1,figsize=plt.figaspect(0.75))
N_med_in, prob, lambda_light,lambda_med=1., 0.28, 8.81, 6.3
csi,y=np.linspace(0,20,100), np.linspace(0,5,50)
N_light=lambda x: N_med_in*prob*(lambda_light/(lambda_light-lambda_med))*\
((np.e)**(-x/lambda_light)-(np.e)**(-x/lambda_med))
N_med= lambda x: N_med_in*(np.e)**(-x/lambda_med)
ratio_exp=0.25
csi_escape=((lambda_med*lambda_light)/(lambda_light-lambda_med))*(np.log((ratio_exp/(prob))*\
((lambda_light-lambda_med)/(lambda_light))+1))
#print(csi_escape)

plt.plot(y*0+csi_escape,y,"r--",\
label="$\\xi_{{\mathrm{{esc}}}}=${}$\:\mathrm{{g}}\:\mathrm{{cm}}^{{-2}}$".format(PowerTen(csi_escape)) )
plt.plot(csi,N_light(csi),'k-',label="$N_{\\mathcal{L}}(\\xi)$")
plt.plot(csi, N_med(csi), "k--",label="$N_{\\mathcal{M}}(\\xi)$" )
plt.xticks(np.arange(min(csi), max(csi)+1, 2.0))
plt.minorticks_on()
plt.axis([0.,20.,0.,1.])
plt.xlabel('$\\xi\:[\mathrm{g}\:\mathrm{cm}^{-2}]$',fontsize=25)
plt.ylabel('$N\:\mathrm{abundances}$',fontsize=25)
plt.legend(loc="best",frameon=False,prop={'size': 23})
plt.tight_layout()
plt.tick_params(labelsize=25)
plt.savefig("escape_time.pdf",format="pdf")		
subprocess.Popen(["evince","escape_time.pdf"])
