\contentsline {chapter}{Introduction}{v}{chapter*.2}%
\contentsline {chapter}{\numberline {1}Relativistic kinematics in a nutshell}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Lorentz transformations}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Invariant Mass}{3}{section.1.2}%
\contentsline {paragraph}{Example: Fixed target experiment}{4}{section*.5}%
\contentsline {paragraph}{Example: Two colliding particles}{4}{section*.6}%
\contentsline {chapter}{\numberline {2}Interactions of astroparticles}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Interactions of particles}{7}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}The cross section}{7}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}The scattering probability}{8}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}The mean free path}{9}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}The range}{11}{subsection.2.1.4}%
\contentsline {section}{\numberline {2.2}Interactions of Astroparticles}{11}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Hadronic interactions}{13}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Photons interactions}{14}{subsection.2.2.2}%
\contentsline {paragraph}{A useful tool}{15}{section*.15}%
\contentsline {subsection}{\numberline {2.2.3}Neutrinos}{15}{subsection.2.2.3}%
\contentsline {paragraph}{\textbf {NB}}{16}{section*.16}%
\contentsline {section}{\numberline {2.3}Particles detection}{20}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Charged particles}{20}{subsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.1.1}Ionization energy loss}{20}{subsubsection.2.3.1.1}%
\contentsline {paragraph}{The Landau distribution}{21}{section*.22}%
\contentsline {paragraph}{Validity limits for the Bethe-Bloch equation}{22}{section*.25}%
\contentsline {subsubsection}{\numberline {2.3.1.2}Electrons and positrons}{25}{subsubsection.2.3.1.2}%
\contentsline {subsubsection}{\numberline {2.3.1.3}High-Energy Radiation Effects}{26}{subsubsection.2.3.1.3}%
\contentsline {subsubsection}{\numberline {2.3.1.4}$\delta $-rays}{27}{subsubsection.2.3.1.4}%
\contentsline {subsubsection}{\numberline {2.3.1.5}Cherenkov radiation}{27}{subsubsection.2.3.1.5}%
\contentsline {paragraph}{Muons}{29}{section*.30}%
\contentsline {subsection}{\numberline {2.3.2}Neutral particles}{31}{subsection.2.3.2}%
\contentsline {subsubsection}{\numberline {2.3.2.1}Photoelectric effect}{32}{subsubsection.2.3.2.1}%
\contentsline {paragraph}{Photomultipliers (PMT)}{33}{section*.35}%
\contentsline {paragraph}{Reminder on scintillators}{33}{section*.37}%
\contentsline {subsubsection}{\numberline {2.3.2.2}Compton scattering}{34}{subsubsection.2.3.2.2}%
\contentsline {subsubsection}{\numberline {2.3.2.3}Pair production}{34}{subsubsection.2.3.2.3}%
\contentsline {chapter}{\numberline {3}Cosmic Rays showers in the atmosphere}{39}{chapter.3}%
\contentsline {section}{\numberline {3.1}The exponential atmosphere}{39}{section.3.1}%
\contentsline {paragraph}{A puntualization on request i)}{49}{section*.49}%
\contentsline {paragraph}{A puntualization on request iii)}{51}{section*.51}%
\contentsline {section}{\numberline {3.2}Electromagnetic showers}{54}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}The Bethe-Heitler model}{54}{subsection.3.2.1}%
\contentsline {paragraph}{NB on MonteCarlo simulations}{57}{section*.56}%
\contentsline {subsection}{\numberline {3.2.2}The Greisen model}{58}{subsection.3.2.2}%
\contentsline {section}{\numberline {3.3}Hadronic showers}{61}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}The hadronic interaction lenght}{61}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}The Heitler splitting approximation}{62}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}The proton-initiated shower}{66}{subsection.3.3.3}%
\contentsline {subsubsection}{\numberline {3.3.3.1}The muon component $N_\mu ^p$}{66}{subsubsection.3.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.3.2}The electromagnetic component $N_{\text {max}}^p$}{67}{subsubsection.3.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.3.3}The depth of the shower maximum}{68}{subsubsection.3.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}The nuclei-induced shower}{69}{subsection.3.3.4}%
\contentsline {chapter}{\numberline {4}Cosmic Rays astrophysics}{75}{chapter.4}%
\contentsline {section}{\numberline {4.1}Historical background}{75}{section.4.1}%
\contentsline {section}{\numberline {4.2}General features}{77}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Differential and integral flux}{77}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}The energy spectrum of primary CRs}{78}{subsection.4.2.2}%
\contentsline {paragraph}{The Solar Flares}{78}{section*.65}%
\contentsline {section}{\numberline {4.3}Overview on direct measurements of CRs}{84}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}A toy detector for Primary Cosmic Rays}{84}{subsection.4.3.1}%
\contentsline {paragraph}{The Interplanetary Monitoring Platform (IMP)}{85}{section*.70}%
\contentsline {subsection}{\numberline {4.3.2}The modern satellite experiments}{85}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}The measured quantities}{86}{subsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.3.1}The chemical composition in our Galaxy}{86}{subsubsection.4.3.3.1}%
\contentsline {paragraph}{The spallation process for lighter nuclei}{88}{section*.75}%
\contentsline {paragraph}{The Cosmic Abundances}{89}{section*.76}%
\contentsline {subsubsection}{\numberline {4.3.3.2}The energy spectrum of protons and nuclei}{89}{subsubsection.4.3.3.2}%
\contentsline {paragraph}{The solar modulation at low energies}{90}{section*.79}%
\contentsline {subsubsection}{\numberline {4.3.3.3}The antimatter in our Galaxy}{92}{subsubsection.4.3.3.3}%
\contentsline {subsubsection}{\numberline {4.3.3.4}Electrons and Positrons fluxes}{94}{subsubsection.4.3.3.4}%
\contentsline {section}{\numberline {4.4}Overview on indirect measurements of CRs}{95}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}The challenge about MC simulations}{96}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}A toy model for an EAS array}{98}{subsection.4.4.2}%
\contentsline {subsection}{\numberline {4.4.3}The measured quantities}{100}{subsection.4.4.3}%
\contentsline {subsubsection}{\numberline {4.4.3.1}The energy of the shower}{101}{subsubsection.4.4.3.1}%
\contentsline {paragraph}{KASKADE}{102}{section*.87}%
\contentsline {subsubsection}{\numberline {4.4.3.2}The direction of the shower axis}{102}{subsubsection.4.4.3.2}%
\contentsline {subsubsection}{\numberline {4.4.3.3}The composition of the shower}{107}{subsubsection.4.4.3.3}%
\contentsline {paragraph}{The Large High Altitude Air Shower Observatory}{108}{section*.93}%
\contentsline {section}{\numberline {4.5}Ultra High Energy Cosmic Rays (UHECRs)}{108}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}General propagation features}{109}{subsection.4.5.1}%
\contentsline {paragraph}{GZK cut-off}{110}{section*.95}%
\contentsline {paragraph}{Bethe-Heitler or Dip mechanism}{113}{section*.97}%
\contentsline {paragraph}{The adiabatic energy loss}{114}{section*.99}%
\contentsline {subsection}{\numberline {4.5.2}Propagation in Magnetic Fields}{114}{subsection.4.5.2}%
\contentsline {paragraph}{What about extragalactic magnetic fields?}{115}{section*.101}%
\contentsline {subsection}{\numberline {4.5.3}Experimental techniques for observing UHECRs}{116}{subsection.4.5.3}%
\contentsline {paragraph}{Fluorescence detectors of UHECRs}{117}{section*.103}%
\contentsline {subsection}{\numberline {4.5.4}Overview on indirect measurements of UHECRs}{118}{subsection.4.5.4}%
\contentsline {subsubsection}{\numberline {4.5.4.1}Single technique detectors}{118}{subsubsection.4.5.4.1}%
\contentsline {subsubsection}{\numberline {4.5.4.2}Large hybrid observatories}{120}{subsubsection.4.5.4.2}%
\contentsline {paragraph}{Pierre Auger Observatory (PAO)}{120}{section*.107}%
\contentsline {paragraph}{Telescope array (TA)}{120}{section*.109}%
\contentsline {subsection}{\numberline {4.5.5}The UHE cut-off mystery and the ankle}{121}{subsection.4.5.5}%
\contentsline {subsection}{\numberline {4.5.6}Searching for UHECRs sources}{124}{subsection.4.5.6}%
\contentsline {paragraph}{Rayleigh Harmonic Analysis}{126}{section*.116}%
\contentsline {paragraph}{The Fermi Large Area Telescope (LAT) Skymap}{128}{section*.118}%
\contentsline {subsection}{\numberline {4.5.7}HE neutrinos in EAS arrays}{129}{subsection.4.5.7}%
\contentsline {section}{\numberline {4.6}Cosmic Rays propagation in the Galaxy}{132}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}The Galactic Accelerators: SN remnants}{133}{subsection.4.6.1}%
\contentsline {subsection}{\numberline {4.6.2}The Standard Model of CRs Acceleration}{136}{subsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.2.1}Second order Fermi mechanism}{137}{subsubsection.4.6.2.1}%
\contentsline {subsubsection}{\numberline {4.6.2.2}First order Fermi mechanism}{140}{subsubsection.4.6.2.2}%
\contentsline {subsubsection}{\numberline {4.6.2.3}The predicted Spectral Index}{143}{subsubsection.4.6.2.3}%
\contentsline {subsubsection}{\numberline {4.6.2.4}SN Remnants as Sites for DSA}{144}{subsubsection.4.6.2.4}%
\contentsline {subsection}{\numberline {4.6.3}The Galactic Diffusion model}{146}{subsection.4.6.3}%
\contentsline {subsubsection}{\numberline {4.6.3.1}The escape time $\tau _{\text {esc}}$}{147}{subsubsection.4.6.3.1}%
\contentsline {subsubsection}{\numberline {4.6.3.2}The Diffusion-Loss equation}{150}{subsubsection.4.6.3.2}%
\contentsline {subsubsection}{\numberline {4.6.3.3}The Leaky Box Model}{152}{subsubsection.4.6.3.3}%
\contentsline {paragraph}{Escape time energy dependence}{153}{section*.129}%
\contentsline {subsubsection}{\numberline {4.6.3.4}The Spectrum at the Sources}{154}{subsubsection.4.6.3.4}%
\contentsline {chapter}{\numberline {5}The Multimessenger Astroparticle Physics}{157}{chapter.5}%
\contentsline {section}{\numberline {5.1}Photons: $\gamma $-rays}{157}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}The Spectral Energy Distribution}{157}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}The Hadronic Model}{158}{subsection.5.1.2}%
\contentsline {paragraph}{NB on the pion bump feature}{160}{section*.132}%
\contentsline {subsubsection}{\numberline {5.1.2.1}Estimate of $\gamma $-ray flux}{161}{subsubsection.5.1.2.1}%
\contentsline {subsection}{\numberline {5.1.3}The Leptonic Model}{162}{subsection.5.1.3}%
\contentsline {subsubsection}{\numberline {5.1.3.1}The Synchrotron Radiation feature}{164}{subsubsection.5.1.3.1}%
\contentsline {subsection}{\numberline {5.1.4}The Extragalactic sources for UHECRs}{165}{subsection.5.1.4}%
\contentsline {paragraph}{NB on Neutron Stars}{166}{section*.135}%
\contentsline {subsection}{\numberline {5.1.5}The High Energy Photons (HE)}{167}{subsection.5.1.5}%
\contentsline {paragraph}{Pulsar Wind Nebula}{171}{section*.143}%
\contentsline {paragraph}{Stellar binaries}{171}{section*.144}%
\contentsline {paragraph}{Gamma Ray Bursts (GRBs)}{173}{section*.145}%
\contentsline {subsection}{\numberline {5.1.6}The Very High Energy Photons (VHE)}{173}{subsection.5.1.6}%
\contentsline {subsubsection}{\numberline {5.1.6.1}The Fermi Mission}{174}{subsubsection.5.1.6.1}%
\contentsline {subsubsection}{\numberline {5.1.6.2}The challenges in VHE astronomy}{176}{subsubsection.5.1.6.2}%
\contentsline {subsubsection}{\numberline {5.1.6.3}The experimental techniques for VHE}{178}{subsubsection.5.1.6.3}%
\contentsline {paragraph}{The On-Off technique}{179}{section*.151}%
\contentsline {paragraph}{Gamma-Hadron separation in Cherenkov telescopes}{180}{section*.153}%
\contentsline {subsubsection}{\numberline {5.1.6.4}The modern Cherenkov telescopes: results}{180}{subsubsection.5.1.6.4}%
\contentsline {paragraph}{The High Energy Stereoscopic System (HESS)}{182}{section*.157}%
\contentsline {paragraph}{The Very Energetic Radiation Imaging Telescope Array System (VERITAS)}{182}{section*.158}%
\contentsline {paragraph}{The Major Atmospheric Gamma-ray Imaging Cherenkov (MAGIC)}{182}{section*.159}%
\contentsline {subsubsection}{\numberline {5.1.6.5}The modern EAS arrays: results}{185}{subsubsection.5.1.6.5}%
\contentsline {paragraph}{MILAGRO}{185}{section*.166}%
\contentsline {paragraph}{The High-Altitude Water Cherenkov Observatory (HAWC)}{185}{section*.167}%
\contentsline {section}{\numberline {5.2}High Energy Neutrinos}{186}{section.5.2}%
\contentsline {paragraph}{The SN 1987 A}{187}{section*.172}%
\contentsline {subsection}{\numberline {5.2.1}Neutrinos production mechanisms}{188}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Neutrinos interactions and cross section}{191}{subsection.5.2.2}%
\contentsline {paragraph}{The neutrino-nucleon cross section}{192}{section*.176}%
\contentsline {paragraph}{Neutrino oscillation}{192}{section*.178}%
\contentsline {subsection}{\numberline {5.2.3}A "guaranteed" source of UHE $\nu $s}{195}{subsection.5.2.3}%
\contentsline {subsection}{\numberline {5.2.4}Towards HE $\nu $s astronomy: an historical overview}{196}{subsection.5.2.4}%
\contentsline {subsection}{\numberline {5.2.5}The IceCube breakthrough}{202}{subsection.5.2.5}%
\contentsline {section}{\numberline {5.3}Gravitational Waves}{204}{section.5.3}%
\contentsline {paragraph}{GRBs as sources of GWs and $\nu $s}{205}{section*.195}%
\contentsline {subsection}{\numberline {5.3.1}Sources of GWs}{205}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}Detecting GWs}{206}{subsection.5.3.2}%
\contentsline {subsubsection}{\numberline {5.3.2.1}The first GWs detectors}{206}{subsubsection.5.3.2.1}%
\contentsline {subsubsection}{\numberline {5.3.2.2}Laser interferometers}{207}{subsubsection.5.3.2.2}%
\contentsline {subsubsection}{\numberline {5.3.2.3}The first detection: GW 150914}{207}{subsubsection.5.3.2.3}%
\contentsline {subsubsection}{\numberline {5.3.2.4}The multimessenger detection: GW 170817}{208}{subsubsection.5.3.2.4}%
\contentsline {chapter}{\numberline {A}Summary and OoM}{213}{appendix.A}%
\contentsline {chapter}{\numberline {B}Pion production}{215}{appendix.B}%
\contentsline {section}{\numberline {B.1}Pion photo-productions}{215}{section.B.1}%
\contentsline {section}{\numberline {B.2}Hadronic processes}{217}{section.B.2}%
