import matplotlib.pyplot as plt
import subprocess
import numpy as np
import matplotlib.cm as cm
import matplotlib.colors as mc
from matplotlib import rc
from matplotlib import rcParams
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text',usetex=True)
rcParams.update({'figure.autolayout': True})


def PowerTen(num,pos=None):
	if (num==0.):
		return "$0$"
	decimal_digits=1
	exponent=int(np.floor(np.log10(abs(num))))
	coeff=round(num/float(10**exponent),decimal_digits)
	if(exponent==-2):
		return "${:.2f}$".format(num)		#TOGLI, È SOLO PER QUESTO ES!!!!!!
	if (exponent==0.):
		return "${:.1f}$".format(coeff)
	if (coeff==1.):
		return r"$10^{{{:d}}}$".format(exponent)
	else:
		return r"${0:.2f}\times10^{{{1:d}}}$".format(coeff,exponent)
		
#to correct the zeros in the log plot	
def mask_array(array):		
	for i in range(0,array.shape[0]):
		for j in range(0,array.shape[1]):
			if(array[i,j]==0.):
				array[i,j]=0.0001
	for i in range(0,array.shape[0]):
		for j in range(0,array.shape[1]):
			if(array[i,j]==0.):
				print("There is still a zero!!")
	return(array)


#INTERACTION_NOT-DECAY_prior to the correction---------------------------------------------------
fig=plt.figure(1)
uno=plt.subplot(221)
lambda_I=100
alphas=np.linspace(0.001,1000,1001)
x=np.linspace(0.001,1000,1001)
y=np.linspace(0,0.45,100)

prob_INTERACT=lambda x, alpha: 1/(alpha+1)*x/((lambda_I)**2)*(np.e)**(-x/lambda_I)
#+(alpha/(alpha+1))/(lambda_I)*(np.e)**(-x/lambda_I)+x/(lambda_I**2)*(alpha/(alpha+1))*(np.e)**(-2*x/lambda_I)
z=np.zeros([len(alphas),len(x)],float)
for i in range(0,len(x)):
	for j in range(0,len(alphas)):
		z[j,i]=prob_INTERACT(x[i],alphas[j])
z_mask=mask_array(z)

#I create the exponents of the ticks for the colorbar
colorbar_ticks=np.arange(np.floor(np.log10(z_mask.min())-1), \
np.ceil(np.log10(z_mask.max())+2),0.5) 		
colorbar_ticks_sci=np.power(10,colorbar_ticks)	

#Contour plot
contour_lines=np.array([5e-2,1e-1,0.33])
clin=plt.contour(x/lambda_I,alphas,z_mask*lambda_I,levels=contour_lines, \
colors="black",linestyles="solid",linewidths=0.15,alpha=0.75)
plt.clabel(clin,colors="k",inline=True,fmt=PowerTen,fontsize=10)
plt.contourf(x/lambda_I,alphas,z_mask*lambda_I, levels=10**np.arange(-6,-0.39,0.05), \
cmap=cm.Blues,norm=mc.LogNorm(vmin=1e-2,vmax=1)) 

#Colorbar
cbar=plt.colorbar(orientation='vertical', ticks=colorbar_ticks_sci, spacing="uniform")
cbar.solids.set_edgecolor("face")

plt.text(3.2,10**2,"$\\frac{\mathrm{d}P_{I,\\bar{d}}^{\pi}}{\mathrm{d}x}=\\frac{1}{\\alpha+1}\:\\frac{x}{(\lambda_I')^2}\:e^{-x/\lambda_I'}$", fontsize=10)
plt.axis([0.1,8,1e-2,1e3])
plt.yscale("log")
plt.ylabel('$\\alpha$',fontsize=10)
plt.xlabel('$x/\lambda_I$',fontsize=10)
plt.tick_params(labelsize=10)
#plt.savefig("INTERACT_contour.pdf",format="pdf")	
#subprocess.Popen(["evince","INTERACT_contour.pdf"])


#INTERACTION_NOT-DECAY_2D-----------------------------------------------------------------
due=plt.subplot(222)
alphas2=np.array([0,1,5,10,1000])										
prob_intCORR=lambda x, alpha: x*(np.e)**(-x/lambda_I)/(lambda_I)**2*(1/(alpha+1))
#colors = cm.RdBu(np.linspace(0, 1, len(alphas)))				#bello!!! colori, li metti con color=colors[i] nel loop.
markers=['-', '--', '-.', ':','-']
for i in range(0,len(alphas2)):
	plt.plot(x/lambda_I,prob_intCORR(x,alphas2[i])*lambda_I,linestyle=markers[i], color="k",\
	label="$\\alpha={}$".format(alphas2[i]))
plt.axis([0,8,0,0.4])
plt.xlabel('$x/\lambda_I$',fontsize=10)
plt.ylabel('$\mathrm{d}P_{I,\\hat{d}}^\pi(x)/\mathrm{d}x$',fontsize=10)
plt.legend(loc="best",frameon=False,prop={'size': 10})
plt.tight_layout()
plt.tick_params(labelsize=10)



#INTERACTION_NOT-DECAY_corrected-----------------------------------------------------------------
#plt.figure(2,figsize=plt.figaspect(0.75))
tre=plt.subplot(223)
prob_INTERACT_CORR=lambda x, alpha: x*(np.e)**(-x/lambda_I)/((lambda_I)**2)*(1/(alpha+1))*(1+alpha*(np.e)**(-x/lambda_I))

z2=np.zeros([len(alphas),len(x)],float)
for i in range(0,len(x)):
	for j in range(0,len(alphas)):
		z2[j,i]=prob_INTERACT_CORR(x[i],alphas[j])
z2_mask=mask_array(z2)

#I create the exponents of the ticks for the colorbar
colorbar_ticks=np.arange(np.floor(np.log10(z2_mask.min())-1), \
np.ceil(np.log10(z2_mask.max())+2),0.5) 		
colorbar_ticks_sci=np.power(10,colorbar_ticks)	

#Contour plot
contour_lines2=np.array([5e-2,1e-1,0.33])
clin2=plt.contour(x/lambda_I,alphas,z2_mask*lambda_I, levels=contour_lines2, \
colors="black",linestyles="solid",linewidths=0.15,alpha=0.75)
plt.clabel(clin2,colors="k",inline=True,fmt=PowerTen,fontsize=10)
plt.contourf(x/lambda_I,alphas,z2_mask*lambda_I, levels=10**np.arange(-6,-0.39,0.05), \
cmap=cm.Blues,norm=mc.LogNorm(vmin=1e-2,vmax=1)) 

#Colorbar
cbar=plt.colorbar(orientation='vertical', ticks=colorbar_ticks_sci, spacing="uniform")
cbar.solids.set_edgecolor("face")

plt.text(4.,3e1,"$\\times(1+\\alpha e^{-x/\lambda_I'})$", fontsize=10)
plt.text(3.2,10**2,"$\\frac{\mathrm{d}P_{I,\\bar{d}}^{\pi}}{\mathrm{d}x}=\\frac{1}{\\alpha+1}\:\\frac{x}{(\lambda_I')^2}\:e^{-x/\lambda_I'}$", fontsize=10)
plt.axis([0.1,8,1e-2,1e3])
plt.yscale("log")
plt.ylabel('$\\alpha$',fontsize=10)
plt.xlabel('$x/\lambda_I$',fontsize=10)
plt.tick_params(labelsize=10)


#INTERACTION_NOT-DECAY_2D-CORR-----------------------------------------------------------------
quattro=plt.subplot(224)									
prob_intCORR2=lambda x, alpha: x*(np.e)**(-x/lambda_I)/((lambda_I)**2)*(1/(alpha+1))*(1+alpha*(np.e)**(-x/lambda_I))
markers=['-', '--', '-.', ':','-']
for i in range(0,len(alphas2)):
	plt.plot(x/lambda_I,prob_intCORR2(x,alphas2[i])*lambda_I,linestyle=markers[i], color="k",\
	label="$\\alpha={}$".format(alphas2[i]))
plt.axis([0,8,0,0.4])
plt.xlabel('$x/\lambda_I$',fontsize=10)
plt.ylabel('$\mathrm{d}P_{I,\\hat{d}}^\pi(x)/\mathrm{d}x$',fontsize=10)
plt.legend(loc="best",frameon=False,prop={'size': 10})
plt.tick_params(labelsize=10)
fig.tight_layout()
plt.savefig("QUATTRO_INT.pdf",format="pdf")	
#subprocess.Popen(["evince","QUATTRO_INT.pdf"])


#INTERACTION_OR_DECAY_corrected-----------------------------------------------------------------
plt.figure(2,figsize=plt.figaspect(0.75))
prob_INTERACTDECAY_CORR=lambda x, alpha: x*(np.e)**(-x/lambda_I)/((lambda_I)**2)*(1/(alpha+1))*(1+alpha*(np.e)**(-x/lambda_I))+(alpha/(alpha+1))*(1/lambda_I)*(np.e)**(-x/lambda_I)
z3=np.zeros([len(alphas),len(x)],float)
for i in range(0,len(x)):
	for j in range(0,len(alphas)):
		z3[j,i]=prob_INTERACTDECAY_CORR(x[i],alphas[j])
z3_mask=mask_array(z3)

#I create the exponents of the ticks for the colorbar
colorbar_ticks=np.arange(np.floor(np.log10(z3_mask.min())-1), \
np.ceil(np.log10(z3_mask.max())+2),0.5) 		
colorbar_ticks_sci=np.power(10,colorbar_ticks)	

#Contour plot
#contour_lines=np.array([5e-2,1e-1,0.33])
#clin=plt.contour(x/lambda_I,alphas,z_mask*lambda_I,levels=contour_lines, \
#colors="black",linestyles="solid",linewidths=0.15,alpha=0.75)
#plt.clabel(clin,colors="k",inline=True,fmt=PowerTen,fontsize=20)
plt.contourf(x/lambda_I,alphas,z3_mask*lambda_I, levels=10**np.arange(-6,0,0.025), \
cmap=cm.Blues,norm=mc.LogNorm(vmin=1e-2,vmax=1)) 

#Colorbar
cbar=plt.colorbar(orientation='vertical', ticks=colorbar_ticks_sci, spacing="uniform")
cbar.solids.set_edgecolor("face")

#plt.text(4.25,3e1,"$+\\frac{x}{(\lambda_I')^2}\\frac{\\alpha}{\\alpha+1}e^{-2x/\lambda_I'}$", fontsize=20)
#plt.text(3.5,10**2,"$\\frac{\mathrm{d}P_{I,\\bar{d}}^{\pi}}{\mathrm{d}x}=\\frac{1}{\\alpha+1}\:\\frac{x}{(\lambda_I')^2}\:e^{-x/\lambda_I'}+$", fontsize=20)
plt.tick_params(labelsize=20)
plt.axis([0.3,8,1e-2,1e3])
plt.yscale("log")
plt.ylabel('$\\alpha$',fontsize=20)
plt.xlabel('$x/\lambda_I$',fontsize=20)
plt.savefig("INTERACT_DECAY_CORR.pdf",format="pdf")	
#subprocess.Popen(["evince","INTERACT_DECAY_CORR.pdf"])


