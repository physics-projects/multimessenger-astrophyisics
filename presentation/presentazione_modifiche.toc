\beamer@endinputifotherversion {3.36pt}
\select@language {italian}
\select@language {italian}
\beamer@sectionintoc {1}{The probability of interaction within $[x,\tmspace +\medmuskip {.2222em}x+\text {d}x]$}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Surviving probability $P(x)$}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Probabilities summmary}{3}{0}{1}
\beamer@subsectionintoc {1}{3}{Decay probability $\text {d}P_d^{\pi }/\text {d}x$}{4}{0}{1}
\beamer@subsectionintoc {1}{4}{A first method}{5}{0}{1}
\beamer@subsectionintoc {1}{5}{A second method}{6}{0}{1}
\beamer@subsectionintoc {1}{6}{Comparison}{7}{0}{1}
\beamer@sectionintoc {2}{The probability to interact or decay $\text {d}P^{\pi }/\text {d}x$}{9}{0}{2}
\beamer@sectionintoc {3}{$x_{\text {-1}}$ at which $1/e$ fraction is surviving }{11}{0}{3}
\beamer@sectionintoc {4}{$x_{\text {max}}$ at which the probability is at maximum }{14}{0}{4}
